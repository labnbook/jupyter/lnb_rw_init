import importSvg from "../style/import.svg";
import exportSvg from "../style/export.svg";
import clearOutputsSvg from "../style/clean.svg";
import jupyterLiteSvg from "../style/jupyterlite.svg";

import { LabIcon } from '@jupyterlab/ui-components';

import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { 
  NotebookActions,
  //NotebookPanel,
  //INotebookModel,
  NotebookModel, 
  INotebookTracker 
} from '@jupyterlab/notebook';


import { 
  IThemeManager,
  ToolbarButton 
} from '@jupyterlab/apputils';

import { ISettingRegistry } from '@jupyterlab/settingregistry';

import { PanelLayout } from '@lumino/widgets';

import { Cell, ICellModel } from "@jupyterlab/cells";

//import { IDisposable, DisposableDelegate } from '@lumino/disposable';

//import { DocumentRegistry } from '@jupyterlab/docregistry';


/**
 * Initialization data for the lnb_rw_init extension.
 */
const plugin: JupyterFrontEndPlugin<void> = {
  id: 'lnb_rw_init:plugin',
  autoStart: true,
  optional: [ISettingRegistry],
  requires: [INotebookTracker, IThemeManager],
  activate: async (app: JupyterFrontEnd, notebookTracker:INotebookTracker, themeManager: IThemeManager, settingRegistry: ISettingRegistry | null) => {

    const id_ld: any = window.frameElement.id.split('ld_code_iframe_').pop();

    if (settingRegistry) {
      settingRegistry
        .load(plugin.id)
        .then(settings => {
          console.log('lnb_rw_init: settings loaded [id_labdoc='+id_ld+'].', settings.composite);
        })
        .catch(reason => {
          console.error('lnb_rw_init: failed to load settings [id_labdoc='+id_ld+'].', reason);
        });
    }

    // Activate theme
    notebookTracker.currentChanged.connect(async(tracker, panel) => {
      if (themeManager.theme !== 'lnb_rw_theme') {
         await themeManager.setTheme('lnb_rw_theme');
      }
      console.log("lnb_rw_theme: lnb_rw_theme is activated [id_labdoc="+id_ld+"]");
    });

    // Send a request to host page
    window.parent.postMessage(
        {
            type: 'init_rw_notebook',
            id_ld: id_ld
        },
    '*');
    console.log("lnb_rw_init -> lnb: init_rw_notebook request [id_labdoc="+id_ld+"]");

    // 1/3 - First promise
    let hostResponsePromise = () => new Promise((resolve, reject) => {
		window.addEventListener('message', event => {
			if (event.data.type === 'init_rw_notebook') {
				resolve(event.data);
			}
		}, {once: true});
	});

    // 2/3 - New promise for timeout purpose
	let timeOutPromise = () => new Promise(function(resolve, reject) {
		setTimeout(function() {
			resolve({});
		}, 5000);
	});

    // 3/3 - Wait for host response, the first of above promises is caught
    let response: any = await Promise.race([hostResponsePromise(), timeOutPromise()]);


    notebookTracker.currentChanged.connect(async(tracker, panel) => {
        if (response.hasOwnProperty('ld_content')){
            if (panel != null){

                //  1 - Initialize content
                const model = new NotebookModel();
                model.fromJSON(JSON.parse(response.ld_content));
                panel.content.model = model;

                // 2 - Include utils functions
                const tag = "utils_functions";
                const cellUtils = {
                    cell: {
                        cell_type: 'code',
                        source: [response.ld_utils_cell],
                        metadata: {"tags": [tag]},
                    }
                };
                // Update utils function cells and hide it
                const codeCell = model.contentFactory.createCodeCell(cellUtils);
                panel.content.widgets.forEach((cell: Cell<ICellModel>, index: number) => {
                    const arrayOfTags = cell.model.metadata.get("tags");
                    if (Array.isArray(arrayOfTags) && arrayOfTags != null && arrayOfTags != undefined){
                        if (arrayOfTags.includes(tag)){
                            panel.content.model!.cells.remove(index);
                        }
                    }
                });
                if (response.ld_utils_cell !== ""){
                    panel.content.model!.cells.insert(0, codeCell);
                    let layout = panel.content.widgets[0].layout as unknown as PanelLayout;
                    for (var i=0; i<5; i++){
                        layout.widgets[i].hide();
                    }
                }

                // 3 - Build clearOutputs button
                const clearOutputsIcon = new LabIcon({
                  name: 'lnb_rw_init:clearOutputs',
                  svgstr: clearOutputsSvg
                });
                const clearOutputs = () => {
                  NotebookActions.clearAllOutputs(panel.content);
                };
                const clearButton = new ToolbarButton({
                  className: 'clear-output-button',
                  label: '',
                  onClick: clearOutputs,
                  tooltip: response.ld_tooltip.clearButton,
                  icon: clearOutputsIcon
                });
                panel.toolbar.insertItem(10, 'clearOutputs', clearButton);

                // 4 - Build import button
                const ImportIcon = new LabIcon({
                  name: 'lnb_rw_init:import',
                  svgstr: importSvg
                });
                const importFiles = () => {
                  const cellImport = {
                    cell: {
                        cell_type: 'code',
                        source: [response.ld_import_cell],
                        metadata: {"tags": []},
                    }
                  };
                  const importCodeCell = model.contentFactory.createCodeCell(cellImport);
                  panel.content.model!.cells.insert(1, importCodeCell);               

                };
                const importButton = new ToolbarButton({
                  className: 'import-files',
                  label: '',
                  onClick: importFiles,
                  tooltip: response.ld_tooltip.importButton,
                  icon: ImportIcon
                });

                panel.toolbar.insertItem(11, 'importFiles', importButton);

                // 5 - Build export button
                const exportIcon = new LabIcon({
                  name: 'lnb_rw_init:export',
                  svgstr: exportSvg
                });
                const exportFiles = () => {
                  const cellExport = {
                    cell: {
                        cell_type: 'code',
                        source: [response.ld_export_cell],
                        metadata: {"tags": []},
                    }
                  };
                  const exportCodeCell = model.contentFactory.createCodeCell(cellExport);
                  panel.content.model!.cells.insert(model.cells.length, exportCodeCell);               

                };
                const exportButton = new ToolbarButton({
                  className: 'export-files',
                  label: '',
                  onClick: exportFiles,
                  tooltip: response.ld_tooltip.exportButton,
                  icon: exportIcon
                });

                panel.toolbar.insertItem(12, 'exportFiles', exportButton);

                // 6 - Send message to host page if toolbar is hidden
                await panel.revealed;
                await panel.sessionContext.ready;

                var toolbarIsHidden = (document.getElementsByClassName("jp-Toolbar-responsive-opener")[0].getElementsByClassName("bp3-minimal")[0] as HTMLElement).offsetParent !== null;
                if (toolbarIsHidden){
                  console.log("lnb_ro_init -> lnb: toolbar_is_hidden [id_labdoc="+id_ld+"]");
                  window.parent.postMessage(
                    {
                        type: 'toolbar_is_hidden', 
                        id_ld: id_ld
                    },
                  '*');
                }

                // 7 - Locate status icon to the right
                var statusDiv = document.getElementsByClassName("jp-Notebook-ExecutionIndicator")[0].parentNode as HTMLElement;
                statusDiv.style.position = "absolute";
                statusDiv.style.right = "12px";

                // 8 - Insert jupyterLite logo
                const jupyterLiteIcon = new LabIcon({
                  name: 'lnb_rw_init:jupyterLite',
                  svgstr: jupyterLiteSvg
                });
                const jupyterLite = () => {
                  window.open('https://jupyterlite.readthedocs.io/en/latest/', '_blank');
                };
                const jupyterLiteButton = new ToolbarButton({
                  className: 'jupyterlite-button',
                  label: '',
                  onClick: jupyterLite,
                  tooltip: response.ld_tooltip.jupyterLiteButton,
                  icon: jupyterLiteIcon
                });
                panel.toolbar.insertItem(13, 'jupyterLite', jupyterLiteButton);
                var jupyterLiteButtonHtml = document.getElementsByClassName("jupyterlite-button")[0] as HTMLElement;
                var jupyterLiteSvgHtml = document.getElementsByClassName("jupyterlite-button")[0].children[0].children[0].children[0] as HTMLElement;
                var jupyterLiterDiv = document.getElementsByClassName("jupyterlite-button")[0].parentNode as HTMLElement;
                jupyterLiteButtonHtml.style.width = "70px";
                jupyterLiteSvgHtml.style.width = "auto";
                jupyterLiteSvgHtml.style.height = "20px";
                jupyterLiterDiv.style.position = "absolute";
                jupyterLiterDiv.style.right = "40px";

                // 9 - Run cell 0 (utils functions)
                panel.content.activeCellIndex = 0;
                NotebookActions.run(panel.content, panel.sessionContext);
                if (typeof document.querySelectorAll('div.jp-Cell-inputWrapper[aria-hidden="true"]')[0] !== 'undefined'){
                    (document.querySelectorAll('div.jp-Cell-inputWrapper[aria-hidden="true"]')[0]
                        .closest('div.jp-Cell') as HTMLElement)
                        .style.display = 'none';
                }
                panel.content.activeCellIndex = 1;

                //10 - Set an event listener in case of resource update
		        window.addEventListener('message', event => {
			        if (event.data.type === 'update_rw_resources') {
				        importButton.onClick = () => {
                            const cellImport = {
                                cell: {
                                    cell_type: 'code',
                                    source: [event.data.ld_import_code],
                                    metadata: {"tags": []},
                                }
                            };
                            const importCodeCell = model.contentFactory.createCodeCell(cellImport);
                            panel.content.model!.cells.insert(1, importCodeCell);
                        };
			        }
		        });

                console.log("lnb_rw_init -> lnb: init_rw_notebook received [id_labdoc="+id_ld+"]");
                
            } else {
                console.error("lnb_rw_init -> lnb: error while initializing the notebook [id_labdoc="+id_ld+"]");
            }
        }
    });
  }
};

export default plugin;
